@extends('adminlte.master')
@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Post Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
              @endif
              <a class="btn btn-primary mb-3" href="/cast/create">Create New Cast</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Title</th>
                      <th>Body</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($posts as $key => $post)
                    <tr>
                      <td>{{  $key + 1  }}</td>
                      <td>{{  $post->title  }}</td>
                      <td>{{  $post->body  }}</td>
                      <td style="display:flex;">
                        <a class="btn btn-info btn-sm" href="/posts/{{$post->id}}">Show</a>
                        <a class="btn btn-default btn-sm ml-2" href="/posts/{{$post->id}}/edit">Edit</a>
                        <form action="/posts/{{$post->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2">
                        </form>
                      </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">No Posts!</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              
            </div>
@endsection