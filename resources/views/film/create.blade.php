@extends('adminlte.master')
@section('content')
    <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create New Cast</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/cast" method="POST">
                    @csrf
                    <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Masukan Judul">
                        @error('judul')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="ringkasan">Ringkasan</label>
                        <input type="text" class="form-control" id="ringkasan" name="ringkasan" value="{{ old('ringkasan', '') }}" placeholder="Masukan ringkasan">
                        @error('ringkasan')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        <input type="text" class="form-control" id="tahun" name="tahun" value="{{ old('tahun', '') }}" placeholder="Masukan tahun">
                        @error('tahun')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="poster">Poster</label>
                        <input type="text" class="form-control" id="poster" name="poster" value="{{ old('poster', '') }}" placeholder="Masukan poster">
                        @error('tahun')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create Cast</button>
                    </div>
                </form>
                </div>
@endsection