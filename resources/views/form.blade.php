@extends('adminlte.master')
@section('content')

    <title>SanberBook - Sign Up</title>

    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf 
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname"><br>

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br>

        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="america">America</option>
            <option value="malaysia">Malaysia</option>
            <option value="cina">Cina</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" name="languagespoken" id="checkbox1">
        <label for="checkbox1">Bahasa Indonesia</label><br>
        <input type="checkbox" name="languagespoken" id="checkbox2">
        <label for="checkbox2">English</label><br>
        <input type="checkbox" name="languagespoken" id="checkbox3">
        <label for="checkbox3">Other</label><br>

        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="5"></textarea><br>
        <input type="submit" value="Sign Up">


    </form>
@endsection