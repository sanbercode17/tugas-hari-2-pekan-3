<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    public function create(){
        return view('film.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            "judul"=>'required|unique:film',
            "ringkasan"=>'required',
            "tahun"=>'required',
            "poster"=>'required'
        ]);

        $query= DB::table('film')->insert([
            "judul" => $request['judul'],
            "ringkasan"=> $request['ringkasan'],
            "tahun"=> $request['tahun'],
            "poster"=> $request['poster']
        ]);

        return redirect('/cast')->with('success','Cast berhasil disimpan!');
    }

    public function index(){
        $posts= DB::table('film')->get();
        // dd($posts);
        return view('film.index', compact('posts'));
    }

    public function show($id){
        $post=DB::table('film')->where('id', $id)->first();
        // dd($post);
        return view('film.show', compact('post'));

    }

    public function edit($id){
        $post=DB::table('posts')->where('id', $id)->first();
        // dd($post);
        return view('film.edit', compact('post'));
    }
    public function update($id, Request $request){
        $request->validate([
            "judul"=>'required|unique:film',
            "ringkasan"=>'required',
            "tahun"=>'required',
            "poster"=>'required'
        ]);

        $query=DB::table('film')
                    ->where('id', $id)
                    ->update([
                        'judul'=> $request['judul'],
                        'ringkasan' => $request['ringkasan'],
                        'tahun' => $request['tahun'],
                        'poster' => $request['poster']
                    ]);
        // dd($post);
        return redirect('/cast')->with('success','Berhasil update cast!');
    }

    public function destroy($id){
        $query= DB::table('film')->where('id', $id)->delete();

        return redirect('/cast')->with('success','Cast Berhasil dihapus!');
    }
}
